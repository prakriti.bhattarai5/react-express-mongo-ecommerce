
# Ecommerce using Mern Stack

This is an ecommerce project developed using Express, Mongodb, Node and React. It has ability to create a user, autenticate and authorize users for different CRUD operations. 


## Installation

Some basic Git commands are:

```bash
  git clone https://gitlab.com/prakriti.bhattarai5/react-express-mongo-ecommerce.git
```

For Frontend 

```bash
  cd frontend
  npm install
  npm start
```

For Backend
```bash
  cd backend 
  npm install
  npm start
```
    
## Questions
If you have any questions about this projects, please contact me directly at prakriti.bhattarai5@gmail.com. You can view more of my projects at https://gitlab.com/prakriti.bhattarai5.