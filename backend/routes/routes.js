const express = require("express");
const app = express();

const user_routes = require("./user.routes");
const auth_routes = require("./auth.routes");
const banner_routes = require("./banner.routes");
const brand_routes = require("./brand.routes");
const category_routes = require("./category.routes");
const product_routes = require("./product.routes")
const cart_routes = require("./cart.routes")

app.use('/users', user_routes)
app.use('/brands', brand_routes)
app.use(auth_routes)
app.use('/banners', banner_routes)
app.use('/categories', category_routes)
app.use('/products', product_routes)
app.use('/cart', cart_routes)
//app.use('/orders', order_routes)

module.exports = app;