const express = require("express")
const router = express.Router()
const uploadImage = require("../app/middleware/uploader.middleware");

const BannerController = require("../app/controllers/banner.controller")
const banner_ctrl = new BannerController();
const AuthMiddleware = require("../app/middleware/auth.middleware");
const loginCheck = require("../app/middleware/auth.middleware");

router.route("/")
      .get(banner_ctrl.listBanner)
      .post(loginCheck,(req, res, next)=>{
            req.dir = 'public/uploads/banner';
            next();
          }, uploadImage.single('image'), banner_ctrl.createBanner)

router.route("/:id")
      .get(banner_ctrl.getBannerById)
      .put(loginCheck,(req, res, next)=>{
            req.dir = 'public/uploads/banner';
            next();
          }, uploadImage.single('image'),banner_ctrl.updateBanner)
      .delete(loginCheck,banner_ctrl.deleteBanner)

module.exports = router