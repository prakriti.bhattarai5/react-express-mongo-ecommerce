const express = require("express");
const UserController = require('../app/controllers/user.controller');
const uploadImage = require("../app/middleware/uploader.middleware");
const user_ctrl = new UserController();

const router = express.Router();

router.route("/")
      .get(user_ctrl.getAllUsers)
      .post((req, res, next)=>{
            req.dir = 'public/uploads/users';
            next();
          }, uploadImage.single('image'),user_ctrl.createUser)

router.route("/:id")
      .get(user_ctrl.getUserById)
      .put((req, res, next)=>{
        req.dir = 'public/uploads/users';
        next();
      }, uploadImage.single('image'),user_ctrl.updateUser)
      .delete()

module.exports = router;