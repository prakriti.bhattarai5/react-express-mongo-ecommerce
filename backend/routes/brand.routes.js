const express = require("express");
const router = require("express").Router();
const BrandController = require('../app/controllers/brand.controller')
const brand_ctrl = new BrandController()
const uploadImage = require("../app/middleware/uploader.middleware");

router.route('/')
      .get(brand_ctrl.listBrand) 
      .post((req, res, next)=>{
            req.dir = 'public/uploads/brand';
            next();
          }, uploadImage.single('image'),brand_ctrl.createBrand)

router.route('/:id')
      .get(brand_ctrl.getBrandById)
      .put((req, res, next)=>{
            req.dir = 'public/uploads/brand';
            next();
          }, uploadImage.single('image'),brand_ctrl.updateBrand)
      .delete(brand_ctrl.deleteBrand)

module.exports = router;
