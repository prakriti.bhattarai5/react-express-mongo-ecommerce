const router = require("express").Router();
const CategoryController = require("../app/controllers/category.controller");
const category_ctrl = new CategoryController();
const uploadImage = require("../app/middleware/uploader.middleware");


router.route('/')
      .get(category_ctrl.listCategory)
      .post((req, res, next)=>{
            req.dir = 'public/uploads/category';
            next();
          }, uploadImage.single('image'),category_ctrl.createCategory)

router.route('/:id')
      .get(category_ctrl.getCategoryById)
      .put((req, res, next)=>{
            req.dir = 'public/uploads/category';
            next();
          }, uploadImage.single('image'),category_ctrl.updateCategory)
      .delete(category_ctrl.deleteCategory)

module.exports = router;
