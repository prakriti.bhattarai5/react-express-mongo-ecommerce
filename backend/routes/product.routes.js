const ProductController = require("../app/controllers/product.controller");
const uploadImage = require("../app/middleware/uploader.middleware");
const router = require("express").Router();
const product_ctrl = new ProductController();
const loginCheck = require("../app/middleware/auth.middleware");

router.route('/:slug')
      .get(product_ctrl.getProductBySlug)

router.route('/')
      .get(product_ctrl.getAllProducts) 
      .get(product_ctrl.getProductBySlug)
      .post(loginCheck,(req, res, next)=>{ 
            console.log(req.body)
            req.dir = 'public/uploads/products';
            next();
      },uploadImage.array('images'),product_ctrl.createProduct)

router.route('/:id')
       .get(product_ctrl.getProductById)
       .put(loginCheck,product_ctrl.updateProduct)
       .delete(loginCheck,product_ctrl.deleteProduct)



module.exports = router;
