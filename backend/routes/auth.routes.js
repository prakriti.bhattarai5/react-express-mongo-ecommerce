const express = require("express");
const router = express.Router();
const uploadImage = require('../app/middleware/uploader.middleware')
const AuthController = require('../app/controllers/auth.controller')
const auth_ctrl = new AuthController();

router.post("/login", auth_ctrl.login)
router.post("/register",(req, res, next) => {
  req.dir = 'public/uploads/users';
  next();
}, uploadImage.single("image"), auth_ctrl.register)

router.post("/facebookLogin", auth_ctrl.facebookLogin);

module.exports = router