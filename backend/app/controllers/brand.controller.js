const BrandModel = require("../models/brand.model");
const BrandService = require("../services/brand.service");
const mongodb = require("mongodb");

class BrandController {

    constructor(){
        this.brand_service = new BrandService();
    }

    listBrand = async(req, res, next) => {
        try{
            const brands = await BrandModel.find({});
            if(brands){
                res.json({
                    result: brands,
                    msg: 'Success',
                    status: true
                })
            }else{
                throw('Brand Not Found')
            }
        }catch(err){
            next({
                error : err
            })
        }
    }

    createBrand = async(req, res, next) => {
        try{
            let data = req.body;
            if(req.file) {
                data.image = req.file.filename
            }
            this.brand_service.validateBrand(data)
            let newBrand = new BrandModel(data);
            newBrand.save().then((suc)=> {
                res.json({
                    msg : 'Brand Created Successfully',
                    result : data
                })
            }).catch((err)=>{
                next({
                    status : 422,
                    error : err
                })
            });
        }catch(err){
            next({
                status : 422,
                error : err
            })
        }
    }

    getBrandById = async(req,res,next) => {
        try{
            console.log(req.params.id)
            let brand = await BrandModel.findById({
                _id : new mongodb.ObjectId(req.params.id)
            });
            console.log(brand)
            if(brand) {
                res.json({
                    result : brand,
                    msg : 'Success',
                    status : true
                })
            }else {
                throw('Brand not found')
            }
        }catch(err){
            console.log(err)
            next({
                error : err
            })
        }  
    }

    updateBrand = async(req,res,next) => {
        try{
            let data = req.data;
            if(req.file) {
                data.image = req.file.filename
            }
            this.brand_service.validateBrand(data)
            let updateBrand = await BrandModel.findByIdAndUpdate(
                {_id : new mongodb.ObjectId(req.params.id)}, 
                data, {new:true,runValidators:true}
            )
            res.json({
                result : updateBrand,
                msg : 'Brand Updated Successfully',
                status : true
            })
            
        }catch(err) {
            next({
                error : err,
            })
        }
    }
    deleteBrand = async(req,res,next) => {
        try{
            let deleteBrand = await BrandModel.findByIdAndDelete({
                _id : new mongodb.ObjectId(req.params.id)
            });
            if(deleteBrand) {
                res.json({
                    result : deleteBrand,
                    msg : "Brand Deleted Successfully",
                    status : true
                })
            }else {
                throw("Banner could not be deleted")
            }
        }catch(err){
            next({
               error : err,
            })
        }
       
    }
 
}

module.exports = BrandController;