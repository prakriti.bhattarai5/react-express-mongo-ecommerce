const CartModel = require("../models/cart.model");
const mongodb = require("mongodb");
const CartService = require("../services/cart.service");
const ProductModel = require("../models/product.model");
const { generateRandomString } = require("../../config/function");

class CartController {

    constructor() {
        this.cart_service = new CartService();
    }
    getCartDetail= async(req,res,next)=>{
        try{
            console.log(req.body)
            let items = req.body
            let product_ids = items.map((obj)=>obj.product_id);
            let product_detail = await ProductModel.find({
                _id:{$in:product_ids}
            })

            let cart_detail = product_detail.map((item)=>{
                let product_id = item._id;
                let qty = 0;
                
                items.map((cartItem)=>{
                    if(product_id.equals(cartItem.product_id)){
                        qty = cartItem['qty']
                    }
                    });
                return {
                    title:item.title,
                    price:item.after_discount,
                    qty:qty,
                    total :qty*item.after_discount
                }
            })
            console.log(cart_detail)
            res.json({
                result:cart_detail,
                status:true,
                msg:"Cart Detail"

            })

        }catch(err){
            console.log(err)
        }
    }

    addItemsInCart =async(req,res,next)=>{
        try{
            let items = req.body
            let product_ids = items.map((obj)=>obj.product_id);
            let product_detail = await ProductModel.find({
                _id:{$in:product_ids}
            })
            let total = 0;
            let cart_detail = product_detail.map((item)=>{
                let ind_total = qty*item.after_discount
                total +=Number(ind_total)
                return {
                    title:item.title,
                    price:item.after_discount,
                    qty:items.map((item)=>CanvasCaptureMediaStreamTrack.product_id === item._id ? cartItem.qty :0),
                    total_amount : ind_total
                }
            })
            let cartInfo = {
                cart_code :generateRandomString(10),
                cart_items:cart_detail,
                customer_id : req.auth_user._id,
                total_amount:total,
                is_paid:false,
                status:"new",

            }
            let cart_obj = new CartModel(cartInfo);
            let status = await cart_obj.save();
            res.json({
                result:cartInfo,
                status:true,
                mag:"Your order has been placed"
            })

        }catch(err){

        }
    }

}
module.exports = CartController