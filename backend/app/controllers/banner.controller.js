const BannerService = require("../services/banner.service");
const BannerModel = require("../models/banner.model");
const mongodb = require("mongodb");

const { default: mongoose } = require("mongoose");
const { validate } = require("../models/user.model");

class BannerController {

    constructor(){
        this.banner_service = new BannerService();
    }

    listBanner = async(req, res, next) => {
        try{
            const banners = await BannerModel.find({});
            
            if(banners){
                res.json({
                    result: banners,
                    status: true,
                    msg: 'Success'

                })
            }else{
                throw('Banners Not Found')
            }
        }catch(err){
            next({
                status : 404,
                error : err
            })
        }
    }

    createBanner = async(req, res, next) => {
        try{
            let data = req.body;
            if(req.file) {
                data.image = req.file.filename
            }
            this.banner_service.validateBanner(data)
            let newBanner = await new BannerModel(data).save();
                if(newBanner){
                    res.json({
                        result : data,
                        msg : "Banner Created Successfully",
                        status: true,
                    })
                }else {
                    throw("Banner could not be created")
                }
            
        }catch(err){
            next({
                error : err
            })
        }
        
    }

    getBannerById = async(req, res, next) => {

        try{
            let banner = await BannerModel.findById({
                _id : new mongodb.ObjectId(req.params.id)
            });
            
            if(banner){
                res.json({
                    result : banner,
                    msg : "Success",
                    status : true
                })
            }else {
                throw("Banner not found")
            }
        }catch(err){
            next({
                error : err,
            })
        }
    }

    updateBanner = async(req, res, next) => {
        try{
            let data = req.body;
            if(req.file) {
                data.image = req.file.filename
            }
            this.banner_service.validateBanner(data)
            let updateBanner = await BannerModel.findByIdAndUpdate({
                _id : new mongodb.ObjectId(req.params.id)
            }, data, {new:true,runValidators:true} )

            if(updateBanner){
                res.json({
                    result : updateBanner,
                    msg : "Banner Updated Successfully",
                    status : true
                })
            }else {
                throw("Banner not found")
            }

        }catch(err){
            next({
                error : err,
            })
        } 
       
    }

    deleteBanner = async(req, res, next) => {
        try{
            let deleteBanner = await BannerModel.findByIdAndDelete({
                _id : new mongodb.ObjectId(req.params.id)
            });
            if(deleteBanner){
                res.json({
                    result : deleteBanner,
                    msg : "Banner Deleted Successfully",
                    status: true
                })
            }else {
                throw("Banner not found")
            }

        }catch(err) {
            next({
                error : err
            })
        }
    }

}

module.exports = BannerController;