const AuthService = require('../services/auth.service')
const db = require('../services/mongodb.service')
const bcrypt = require("bcrypt");
const UserModel = require('../models/user.model');

class AuthController{
    constructor(){
        this.auth_service = new AuthService();
    }
    login = async (req, res, next) => {
        try {
            let data = req.body;
            this.auth_service.loginValidate(data);
            let user = await UserModel.findOne({
                email : data.email,
            });
            if(user) {
                let password = await bcrypt.compare(data.password, user.password);
                console.log(password)
                if(password) {
                    let token = this.auth_service.generateToken(data);
                    res.json({
                                result: user,
                                access_token : token,
                                status : 200,
                                msg : "User logged in successfully",
                        })  
                }else {
                    throw("Credential does not match")
                }
            }else {
                throw("User does not exixts.")
            }
            
        } catch(error){
            next({
                error : error
            })
        }
      
    }

    register = async (req, res, next) => {
        try {
                let data = req.body;
                let userExists = await UserModel.findOne({email: data.email});
                if(userExists){
                    res.json({
                        result : data.email,
                        msg : "Email is already taken",
                        status : false
                    })
                }
                if(req.file) {
                    data.image = req.file.filename
                }
                this.auth_service.validateUser(data)
                data.password = bcrypt.hashSync(data.password, 10);
                
                let newUser = new UserModel(data);
                newUser.save().then((succ) => {
                    res.json({
                        result : data,
                        status : true,
                        msg : "User registered successfully"
                    })
                }).catch((err)=> {
                    next({
                        error : err
                    })
                })
               

        } catch(err) {
           next({
                error : err,
            })
        }
    }

    facebookLogin = (req, res, next) => {
        console.log(req.body);
        // let user = fetch("https://graph.facebook.com/"+req.id+"?fields=id,name,email&access_token="+req.access_token);
        console.log(user);
    }

}


module.exports = AuthController