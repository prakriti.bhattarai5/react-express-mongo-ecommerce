const UserService = require("../services/user.service")
const AuthService = require("../services/auth.service")
const db = require("../services/mongodb.service");
const mongodb = require("mongodb");
const UserModel = require("../models/user.model");

class UserController {

    constructor(){
        this.auth_service = new AuthService();
    }

    getAllUsers = async (req, res, next) => {
        try {
            let selDb = await db();
            if(selDb) { 
                let users = await selDb.collection('users').find().toArray()
                res.json({
                    result : users,
                    status : true,
                    msg : "Fetched"
                })
                
            }else {
                throw('Error connecting database')
            }

        } catch(err) {
            next({
                result : err
            })
        }
    }

    getUserById = async(req, res, next) => {
        try {
            console.log(req.params.id)
            let selDb = await db();

            if(selDb){
                let user =await selDb.collection("users").findOne({
                    _id : new mongodb.ObjectId(req.params.id)
                })
                res.json({
                    result : user,
                    status : true,
                    msg : "User fetched successfully"
                })
            }else {
                throw('Error connecting to Database')
            }
        
        } catch (err) {
            next({
                error : err
            })
        }
    }

    updateUser = async (req, res, next) => {
        try{
            let data = req.body
            let validate = this.auth_service.validateUser(data, true)
            if(req.file) {
                data.image = req.file.filename
            }
            if(validate) {
                next({
                    status : 400,
                    msg : validate
                })
            } else {
                let selDb = await db();
                if(selDb){
                    let update = await selDb.collection("users").updateOne({
                        _id : new mongodb.ObjectId(req.params.id)
                    }, {
                        $set : data
                    })
                    if(update) {
                        res.json({
                            result : data,
                            msg : "User updated successfully",
                            status : true
                        })
                    }else {
                        throw('User could not be updated')
                    }
                }else {
                    throw('Error connecting to Database')
                }
            }
        } catch(err) {
            next({
                error : err
            })
        }

    }

    createUser = async(req, res, next) => {
        try{
            let data = req.body;
            this.auth_service.validateUser(data)

            let newUser = await new UserModel(data).save();
            if(newUser){
                res.json({
                    result : newUser,
                    msg : "Success",
                    status : true
                })
            }else {
                throw("User could not be created")
            }

        }catch(err){
            next({
                error : err
            });

        }
    }

    deleteUser = async(req, res, next) => {
        try{
            let deleteUser = await UserModel.findByIdAndDelete({_id : new mongodb.ObjectId(req.params.id)});
            if(deleteUser){
                res.json({
                    result : deleteUser,
                    status : true,
                    msg : "Success"
                })
            }else {
                throw("User not found")
            }

        }catch(err) {
            next({
                error : err
            })
        }
    
    }
}
module.exports = UserController