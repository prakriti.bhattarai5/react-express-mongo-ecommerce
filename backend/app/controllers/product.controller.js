const ProductModel = require("../models/product.model");
const ProductService = require("../services/product.service");
const mongodb = require("mongodb");
const { default: slugify } = require("slugify/slugify");
const { generateRandomString } = require("../../config/function");
const { ObjectId } = require('mongoose');

class ProductController {

    constructor() {
        this.product_service = new ProductService();
    }

    getAllProducts = async(req, res, next) => {
        try{
            let products = await ProductModel.find({});
            if(products) {
                res.json({
                    result : products,
                    status : true,
                    msg : "Products fetched"
                })
            }else {
                throw("Products not found!!");
            }
        }catch(err){
            next(err)
        }
    }

    getProductById = async(req, res, next) => {
        try{
            let product = await ProductModel.findById({
                _id : new mongodb.ObjectId(req.params.id)
            });
            if(product) {
                res.json({
                    result : product,
                    msg : 'success',
                    status : true
                })
            }else {
                throw('Product not found')
            }
        }catch(err){
            next(err)
        }
    }

    createProduct = async(req, res, next) => {
        try{
            let data = req.body;
            console.log(data);
            let images = [];

            if(req.files){
                req.files.map((item)=> {
                    images.push(item.filename);
                    data.images = images;
                });
            }
            if(!data.slug){
               let slug = slugify(data.title,{replacement:'-',lower:true});
                //data.slug = this.generateUniqueSlug(slug);
                data.slug = slug
            }
            if(!data.category){
                data.category=null;
            }
            if(!data.seller){
                data.seller=null;
            }
            if(!data.brand){
                data.brand=null;
            }
            let discount ={
                discount_type : 'percent',
                discount_value:0
            }
            if(data.discount_type){
                discount.discount_type = data.discount_type,
                delete data.discount_type;
            }
            
            if(data.discount_value){
                discount.discount_value = data.discount_value,
                delete data.discount_value;
            }
            data.discount = discount;
            if(data.discount.discount_type === 'percent'){
                data.after_discount = data.price - data.discount.discount_value/100;
            }else {
                data.after_discount = data.price - data.discount.discount_value;
            }


            this.product_service.validateProduct(data);
            let create_product = await new ProductModel(data).save();
            if(create_product) {
                res.json({
                    result : data,
                    status : true,
                    msg : "Product created successfully"
                })
            }else { 
                throw("Product could not be created")
            }
        }catch(err){
            console.log(err)
            next(err)
        }
    }

    updateProduct = async(req, res, next) => {
        try{
            let updateProduct = await ProductModel.findByIdAndUpdate(
                req.params.id, req.body, {
                    new : true,
                    runValidators : true} 
            );
            if(updateProduct){
                res.json({
                    result : updateProduct,
                    msg : 'Updated Successfully',
                    status : true
                })
            }
         
        }catch(err) {
            next(err)
        }
    }
    getProductBySlug = async(req,res,next)=>{
        try{

            // Retrieve the slug parameter from the request
            let slug = req.params.slug;
            const id = new ObjectId(slug);
            console.log(id)
            // Retrieve the product from the database by its slug
            let product = await ProductModel.findOne({
                slug: slug
            });

            if(product) {
                res.json({
                    result : product,
                    msg : "Success",
                    status : true
                })
            }else {
                throw("Product could not be found")
            }
        }catch(err){
            next(err)
        }
    }
    deleteProduct = async(req, res, next) => {
        try{
            let deleteProduct = await ProductModel.findByIdAndDelete(req.params.id);
            if(deleteProduct) {
                res.json({
                    result : deleteProduct,
                    msg : "Success",
                    status : true
                })
            }else {
                throw("Product could not be deleted")
            }
        }catch(err){
            next(err)
        }
    }
    // generateUniqueSlug=async(slug)=>{
    //     let exists = await ProductModel.findOne({
    //         slug:slug
    //     });
    //     if(exists){
    //         slug = slug+Date.now();
    //         await this.generateUniqueSlug(slug);
    //     }else {
    //         return slug;
    //     }
    // }

   getProductByCategory = async()=>{

    
   }

}
module.exports = ProductController