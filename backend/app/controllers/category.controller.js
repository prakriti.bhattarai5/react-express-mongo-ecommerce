const CategoryModel = require("../models/category.model");
const CategoryService = require("../services/category.service");
const mongodb = require("mongodb");

class CategoryController{

    constructor(){
        this.category_service = new CategoryService();
    }

    listCategory = async(req,res,next) => {
        try{
            let categories = await CategoryModel.find({});
            if(categories){
                res.json({
                    result: categories,
                    status: true,
                    msg: 'Success'

                })
            }else{
                throw('Categories Not Found')
            }
        }catch(err){
            next({
                status : 404,
                error : err
            })
        }
        
    }

    createCategory = async(req, res, next) => {
        try{
            let data = req.body;
            if(req.file) {
                data.image = req.file.filename;
            }
            this.category_service.validateCategory(data);
            let newCategory = new CategoryModel(data);
            newCategory.save().then((suc)=> {
                res.json({
                    result : data,
                    msg : 'Category created successfully',
                    status : true
                })
            }).catch((err)=>{
                next({
                    error : err
                })
            });
        }catch(err){
            next({
                error : err
            })
        }
    }

    getCategoryById = async(req, res, next) => {
        try{
            let category = await CategoryModel.findById({
                _id : new mongodb.ObjectId(req.params.id)
            });
            if(category) {
                res.json({
                    result : category,
                    msg : 'success',
                    status : true
                })
            }else {
                throw('Category not found')
            }
        }catch(err){
            next({
                error : err
            })
        }
    }

    updateCategory = async(req, res, next) => {
        try{
            let data = req.body;
            if(req.file) {
                data.image = req.file.filename
            }
            this.category_service.validateCategory(data);
            let updateCategory = await CategoryModel.findByIdAndUpdate(
                {
                    _id : new mongodb.ObjectId(req.params.id)
                }, data, {
                    new : true,
                    runValidators : true} 
            )
            res.json({
                result : updateCategory,
                msg : 'Category Updated Successfully',
                status : true
            })
    
        }catch(err) {
            next({
                error : err
            })
        }
    }
    
    deleteCategory = async(req, res, next) => {
        try{
            let deleteCategory = await CategoryModel.findByIdAndDelete({
                _id : new mongodb.ObjectId(req.params.id)
            });
            if(deleteCategory) {
                res.json({
                    result : deleteCategory,
                    msg : "Category Deleted Successfully",
                    status : true
                })
            }else {
                throw("Category could not be deleted")
            }
        }catch(err){
            next({
               error : err
            })
        }
    }

}
module.exports = CategoryController