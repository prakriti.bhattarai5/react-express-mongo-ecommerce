const mongodb = require("mongodb")
const MongoClient = mongodb.MongoClient;
const CONFIG = require('../../config/config')

const db = () => {
    return new Promise((resolve, reject)=>{
        MongoClient.connect(CONFIG.DB_URL, (err, client)=> {
            if(err) {
                reject(err);
            }else {
                const dbselected = client.db(CONFIG.DB_NAME)
                resolve(dbselected);
            }
        })
    })
   
}

module.exports = db