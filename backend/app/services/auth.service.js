const UserModel = require("../models/user.model")
const CONFIG = require("../../config/config")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

class AuthService {

    loginValidate = (data) => {
        let msg = {};
        if(!data.email || !data.password){
            msg = "Credentials are required";
        }else{
            msg = null;
        }
        return msg;
    }

    validateUser = (data, is_update = false) => {
        let msg = {};
        if(!data.name) {
            msg.name = "Name is required"
        }
        if(!data.email) {
            msg.email = "Email is required"
        }
        if(!data.password) {
            msg.password = "Password is required"
        }
        if(data.role) {
            if(!data.role == 'admin' || !data.role == 'seller') {
                msg.role = "Role must be admin or seller"
            }
        }else{
            msg.role = "Role is required"
        }
        if(Object.keys(msg).length !== 0) {
            throw msg;
        }
        return null;
    }

    login = async (data) => {
        try {
            
        } catch(err){
           throw err;
        }
        
    }   

    register = async (data) => {

    }

    generateToken = (data) => {
        let token = jwt.sign(data, CONFIG.JWT_SECRET);
        return token;
    }
}

module.exports = AuthService