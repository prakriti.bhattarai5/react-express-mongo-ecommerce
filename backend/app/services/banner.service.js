class BannerService {

    validateBanner = (data) => {
        let msg = {};
    
        if(!data.title){
            msg.title = 'Title is required'
        }
        if(!data.status){
            msg.status = 'Status is required'
        }
        if(!data.slug){
            msg.slug = 'Slug is required'
        }

        if(Object.keys(msg).length !== 0){
            throw msg;
        }
        return null;
    }

}

module.exports = BannerService