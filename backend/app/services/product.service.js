class ProductService{

    validateProduct = (data) => {
        let msg = {};
        if(!data.title) {
            msg.title = "Name is required";
        }
        if(!data.slug) {
            msg.slug = "Slug is required";
        }
        if(!data.price) {
            msg.price = "Price is required";
        }else if(data.price){
            if(data.price <= 1){
                msg.price = "Price must be greater than 1";
            }
        }
        if(!data.is_featured) {
            msg.is_featured = "Is Featured is required";
        }
        if(!data.description) {
            msg.description = "Description is required";
        }
        if(!data.status) {
            msg.status = "Status is required";
        }
        if(Object.keys(msg).length !== 0){
            throw msg;
        }
        return null;
    }
}

module.exports = ProductService