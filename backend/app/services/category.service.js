class CategoryService {

    validateCategory = (data) => {
        let msg = {};
        if(!data.title) {
            msg.title = "Title is required";
        }
        // if(!data.status) {
        //     msg.status = "Status is required";
        // }else if(data.status !== "active" || data.status !== "inactive"){
        //     msg.status = "Status must be active or inactive"
        // }
        if(Object.keys(msg).length !== 0){
            throw msg;
        }
        return null;
    }
}

module.exports = CategoryService