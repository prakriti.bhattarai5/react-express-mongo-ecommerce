const Config = require("../../config/config")
const mongoose = require('mongoose');

mongoose.connect(Config.DB_URL+"/"+Config.DB_NAME,(err) => {
    if(err) {
        console.log("Error connecting database", err)
    } else {
        console.log('Mongodb connected successfully')
    }
})