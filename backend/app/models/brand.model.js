const mongoose = require("mongoose");

const BrandSchemaDef = new mongoose.Schema({
    title : {
        type : String,
        required : [true, "Title is required"]
    },
    slug : {
        type : String,
        required : [true,"Slug is required"]
    },

    status : {
        type : String,
        required : [true, "Status is required"]
    },
    image : {
        type : String, 
        default : null
    }
})

const BrandModel = mongoose.model('Brand', BrandSchemaDef)

module.exports = BrandModel