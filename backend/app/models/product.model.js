const mongoose = require("mongoose")

const ProductSchemaDef = new mongoose.Schema({
    title : {
        required : true,
        type : String,

    },
    
    slug : {
        required: true,
        type: String,
        unique: true
    },
    price : {
        type: Number,
        min:1,
        required: true
    },
    quantity : {
        type: Number,
        min:0,
        required: true
    },
    discount : {
        discount_type : {
            type:String,
            enum :["percent",'flat'],
            default:"percentage"
        },
        discount_value:{
            type:Number,
            min:0
        }
    },
    after_discount : {
        type:Number,
    },
    category : [{
        type: mongoose.Types.ObjectId,
        ref : "Category"
    }],
    description : {
        type : String,
    },
    images : [{
        type:String
    }],
    seller : {
        type : mongoose.Types.ObjectId,
        ref : "User",
        default:null
    },
    status : {
        type : String,
        required: true,
        enum : ['active','inactive','out-of-stock'],
        default : 'inactive'
    },
    brand : {
        type:mongoose.Types.ObjectId,
        ref:"Brands"
    },
    is_featured : {
        required : true,
        type : Boolean,
        default : false
    },
    stock:{
        type:Number,
        min:0
    }
},{
    timestamps : true,
    autoCreate : true,
    autoIndex : true
});

const ProductModel = mongoose.model('Product', ProductSchemaDef)

module.exports = ProductModel