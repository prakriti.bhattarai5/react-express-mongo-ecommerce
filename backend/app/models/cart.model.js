const mongoose = require("mongoose")

const CartSchemaDef = new mongoose.Schema({
    cart_code : {
        type : String,
        required : true
    },
    cart_items : [{
        product_id : {
            type:mongoose.Types.ObjectId,
            ref:"products",

        },
        qty:Number,
        total_amount : Number
    }],
    customer_id:{
        type :mongoose.Types.ObjectId,
        ref:"users"
    },
    total_amount:Number,
    is_paid:{
        type:Boolean,
        default:false
    },
    status:{
        type:String,
        enum:['new','panding','deliveered','canceled']
    }
    
},{
    timestamps : true,
    autoCreate : true,
    autoIndex : true
})

const CartModel = mongoose.model('Cart', CartSchemaDef)

module.exports = CartModel