const mongoose = require("mongoose");

const UserSchemaDef = new mongoose.Schema({
    name : {
        type : String,
        required : [true,"Name is required"],
    },
    email:{
        type: String,
        required : true,
        unique: [true, "Email should be unique"]
    },
    password:{
        type: String,
        required: [true, "Password is required"]

    },
    role:{
        type : String,
        enum: ['admin', 'seller', 'customer'],
        default : 'customer'
    },
    status: {
        type: String,
        enum: ['active','inactive'],
        default: 'active'
    },
    image: {
        type : String,
        default: null
    }
});

const UserModel = mongoose.model('User', UserSchemaDef);

module.exports = UserModel;