const mongoose = require("mongoose")

const BannerSchemaDef = new mongoose.Schema({
    title : {
        type : String,
        required : [true,"Title is required"]
    },
    slug : {
        type : String,
        required : [true, "Slug is required"]
    },
    status : {
        type : String,
        required : [true, "Status is required"]
    },
    image : {
        type : String, 
        default : null
    }
})

const BannerModel = mongoose.model('Banner', BannerSchemaDef)

module.exports = BannerModel