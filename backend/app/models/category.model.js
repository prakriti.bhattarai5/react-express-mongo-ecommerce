const mongoose = require("mongoose");

const CategorySchemaDef = new mongoose.Schema({
    title : {
        type : String,
        required : [true, 'Title is required']
    },
    slug : {
        type : String,
        required : [true, 'Slug is required']
    },
    status : {
        type : String,
        required : [true, 'Status is required']

    },
    image : {
        type : String,

    }

})

const CategoryModel = mongoose.model('Category', CategorySchemaDef)

module.exports = CategoryModel