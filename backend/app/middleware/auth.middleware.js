const jwt = require("jsonwebtoken")
const CONFIG = require("../../config/config")
const UserModel = require("../models/user.model")

const loginCheck = async (req, res, next) => {
    let token = '';
    if(req.headers['authorization']) {
        token = req.headers['authorization']
    }
    if(req.headers['x-xsrf-token']) {
        token = req.headers['x-xsrf-token']
    }
    if(req.headers['token']) {
        token = req.headers['token']
    }
    if(!token) {
        next({
            status : 401,
            msg : "Unauthenticated"
        })
    }else {
        try{
            let token_parts = token.split(' ');
            token = token_parts[token_parts.length-1]

            let data = jwt.verify(token, CONFIG.JWT_SECRET)
            if(data) {
                let user = await UserModel.findOne({
                    email : data.email,
                });
                if(user) {
                    req.auth_user = user
                    next();

                } else {
                    next({
                        status : 401,
                        msg : "Token expired or user does not exists"
                    })
                }
            }else {
                next({
                    status : 401,
                    msg : "Unauthorized: Token Mismatched"
                })
            }
        }catch(err){
            next({
                error : err
            })
        }
    }
}
module.exports = loginCheck