const multer = require("multer");
const fs = require('fs');

const myStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        
        let path = 'public/uploads';
        if(req.dir) {
           path = req.dir
        }
        if(!fs.existsSync(path)) {
            fs.mkdirSync(path,{
                recursive:true
            });
        }
      cb(null, path)
    },
    filename: function (req, file, cb) {
      const filename = Date.now()+'-'+file.originalname;
      cb(null, filename)
    }
})

const imageFilter = (req, file, cb) => {
  let exts = file.originalname.split(".");
  let ext = exts.pop();
  let allowed = ["jpg",'png','gif','bmp','svg','webp','jpeg']

  if(allowed.includes(ext.toLowerCase())){
    cb(null, true)
  } else {
    cb({
      status : 400,
      msg : "Unsupported file format"
    }, null)
  }

}
const upload = multer({ 
    storage: myStorage,
    fileFilter : imageFilter
})

module.exports = upload
