const express = require("express");
const app = express();
var bodyParser = require('body-parser');
const routes = require("./routes/routes")
const cors = require("cors");
require("./app/services/mongoose.service")
/** Require multer */
app.use(cors());
app.use(express.json());
app.use(bodyParser.json()); 

app.use(express.urlencoded({extended:true}))
// for parsing application/json

app.use(express.static('public'))
app.use('/api/v1', routes)

app.set("view engine", "pug")
app.set("views", process.cwd() + "/views")
app.get('/prakriti', (req, res, next)=>{
    res.render("profile");
})
// app.use((req,res,next)=> {
//     next({

//     });
// })

app.use((error, req, res, next)=> {
    let status_code = error.status || 500;
    let msg = error.msg || error;
    console.log(error)
    res.status(status_code).json({
        msg : msg,
        result : null,
        status : false
    })
})

app.listen(3005, 'localhost', (err)=>{
    if(err) {
        console.log("Error Listening port");
    } else {
        console.log("Listening port",3005);
    }
})