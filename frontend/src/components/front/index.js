import NavbarComponent from "./navbar.component";
import FooterComponent from "./footer.component";

const Front = {
    NavbarComponent,
    FooterComponent
}

export default Front;