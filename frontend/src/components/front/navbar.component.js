import { useState } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

const NavbarComponent = () => {
    let [totalItems,setTotalItems]= useState(0);

    totalItems = useSelector((store)=>{
        console.log(store.cart.cart)
        let cart = store.cart;
        let total = 0;
        cart.cart && cart.cart.map((item)=>{
            total += Number(item.qty)
        })
        return total;
    })
    return (<>
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand to="/" className="text-light">High Fashion</Navbar.Brand>
                <Nav className="me-auto">
                    <NavLink className="nav-link" to="/">Home</NavLink>
                    <NavLink className="nav-link" to="/about-us">About</NavLink>
                    <NavLink className="nav-link" to="/categories">Shop</NavLink>
                    <NavLink className="nav-link" to="/brands">Brand</NavLink>
                    <NavLink className="nav-link" to="/brands">Category</NavLink>
                 
                    <Navbar.Toggle />   
                </Nav>
                <Nav className="justify-content-end text-light">
                    <NavLink className="nav-link" to="/login">Login</NavLink>
                    <NavLink className="nav-link" to="/register">Register</NavLink>
                </Nav>
                <Nav className="justify-content-end text-light">
                    <NavLink className="nav-link" to="/cart">Cart({totalItems})</NavLink>
                </Nav>
            </Container>
        </Navbar>
    </>)

}

export default NavbarComponent;