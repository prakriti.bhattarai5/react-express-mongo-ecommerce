import { Col, Container, Row } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import Breadcrumbs from "./breadcrumbs";

const TopElements = (props) => {
    const name = props.name;
    const type = props.type;

    return(<>
        <Container>
            <div className="mt-3 mb-3">
                <Row>
                    <Col sm={9}>{name && <h3>{name} {type}</h3>}</Col>
                    <Col sm={3}><Breadcrumbs/></Col>
                </Row>
                
            </div> 
        </Container>
         
    </>)
}

export default TopElements;