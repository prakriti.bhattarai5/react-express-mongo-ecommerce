import { Link, useLocation } from "react-router-dom";
const Breadcrumbs = () => {
    const location = useLocation();

    let currentLink = ''
    const crumbs = location.pathname.split('/').filter(crumb => crumb !== '').slice(0,3).map((crumb,index)=>{
        currentLink += `/${crumb}`
        return (
                <li className="breadcrumb-item" key={index}><Link to={currentLink}>{crumb}</Link> </li>
        );
    });
    
    return(<>
        <ol className="breadcrumb mb-4">
            {crumbs}
        </ol>

    </>)
}

export default Breadcrumbs;