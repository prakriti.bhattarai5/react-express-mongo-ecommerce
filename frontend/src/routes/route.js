import { useEffect, useState } from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import AdminPages from "../pages/admin";
import FrontPage from "../pages/front";
import AdminLayout from "../pages/layouts/admin-layout.page";
import FrontLayout from "../pages/layouts/front-layout.page";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css"

const Routing = () => {
    const [authenticated, setAuthenticated] = useState(true);

    useEffect(() => {
        // Check if user info is present in local storage, if not then set the authenticated state to false
        let token = localStorage.getItem('access_token')
        if (!token) {
        setAuthenticated(false);
        }
    }, []);

    return(

        <BrowserRouter>
        <ToastContainer/>
            <Routes>
                <Route path="/" element={<FrontLayout/>}>
                    <Route index element={ <FrontPage.HomePage/> } />    
                    <Route path="login" element={ <FrontPage.LoginPage/> } />
                    <Route path="register" element={ <FrontPage.RegisterPage/> } />
                    <Route path="cart" element={ <FrontPage.Cart/> } />
                    <Route path="products/:slug" element={ <FrontPage.ProductSingle/> } />
                    <Route path="*" element={ <FrontPage.ErrorPage/> } />
                </Route>
                {authenticated} ?? 
                <Route path="/admin" element={<AdminLayout/>}>
                    <Route index element={<AdminPages.AdminDashboard/>}></Route>
                    <Route path="banner" element={ <AdminPages.Banner.BannerLayout/> }>
                        <Route index element={ <AdminPages.Banner.BannerList/> } />
                        <Route path="create" element={ <AdminPages.Banner.BannerCreate />}></Route>
                        <Route path="edit/:id" element={ <AdminPages.Banner.BannerEdit />}></Route>
                    </Route>
                    <Route path="brand" element={ <AdminPages.Brand.BrandLayout/> }>
                        <Route index element={ <AdminPages.Brand.BrandList/> } />
                        <Route path="create" element={ <AdminPages.Brand.BrandCreate />}></Route>
                        <Route path="edit/:id" element={ <AdminPages.Brand.BrandEdit />}></Route>
                    </Route>
                    <Route path="category" element={ <AdminPages.Category.CategoryLayout/> }>
                        <Route index element={ <AdminPages.Category.CategoryList/> } />
                        <Route path="create" element={ <AdminPages.Category.CategoryCreate />}></Route>
                        <Route path="edit/:id" element={ <AdminPages.Category.CategoryEdit />}></Route>
                    </Route>
                    <Route path="user" element={ <AdminPages.User.UserLayout/> }>
                        <Route index element={ <AdminPages.User.UserList/> } />
                        <Route path="create" element={ <AdminPages.User.UserCreate />}></Route>
                        <Route path="edit/:id" element={ <AdminPages.User.UserEdit />}></Route>
                    </Route>
                    <Route path="product" element={ <AdminPages.Product.ProductLayout/> }>
                        <Route index element={ <AdminPages.Product.ProductList/> } />
                        <Route path="create" element={ <AdminPages.Product.ProductCreate />}></Route>
                        <Route path="edit/:id" element={ <AdminPages.Product.ProductEdit />}></Route>
                    </Route>
                </Route>

            </Routes>
        </BrowserRouter>
    )
}
export default Routing;