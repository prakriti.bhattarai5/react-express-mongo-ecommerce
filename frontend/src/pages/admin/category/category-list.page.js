import { useEffect, useState } from "react";
import { Col, Row, Table } from "react-bootstrap"
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import TopElements from "../../../components/common/top_elements";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";

const CategoryList = () => {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
      async function fetchData() {
        const response = await httpGetRequest("/categories");
        setCategories(response.result)
      }  
      fetchData();
    }, []);

    const handleDelete = async (itemId) => {
        try {
          // Send a HTTP DELETE request to delete the item from the database
          const response = await httpDeleteRequest(`/categories/${itemId}`);
          // Update the state of the component to remove the deleted item
          setCategories(categories.filter((item) => item._id !== itemId));
          if(response.status){
            toast.success(response.msg);
          }
        } catch (error) {
            toast.error(error.msg);

        }
      };
  
     return(<>
         <div className="container-fluid px-4">
            <TopElements name="Category" type="create"/>
                
             <div className="card mb-4">
                <Row className="m-2">
                    <Col sm={10}></Col>
                    <Col sm={2}><Link to={'create'} className="btn-sm btn-primary" width={100}> <i className="fa-solid fa-plus"></i> Create</Link></Col>
                </Row>
                 <div className="card-body">
                     <Table striped bordered hover>
                         <thead>
                             <tr>
                             <th>#</th>
                             <th>Image</th>
                             <th>Title</th>
                             <th>Status</th>
                             <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             {categories && categories.map((item,index) => (
                                 <tr key={index}>
                                     <td>{++index}</td>
                                     <td>{item.image && <img src={`http://127.0.0.1:3005/uploads/category/${item.image}`} alt="item" height="40" width="40"/>}</td>
                                     <td>{item.title}</td>
                                     <td>{item.status}</td>
                                     <td>
                                        <Link to={`edit/${item._id}`}> <i className="fa-solid fa-pen-to-square"></i> </Link> |  
                                        <a href="#" onClick={() => handleDelete(item._id)}>
                                             <i className="fa-solid fa-trash"></i>
                                        </a>
                                    </td>
                                 </tr>
                             ))}
                         </tbody>
                     </Table>
                 </div>
             </div>
         </div>
     </>)
}

export default CategoryList