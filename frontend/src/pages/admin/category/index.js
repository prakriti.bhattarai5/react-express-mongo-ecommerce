import CategoryCreate from "./category-create.page"
import CategoryEdit from "./category-edit.page"
import CategoryLayout from "./category-layout.page"
import CategoryList from "./category-list.page"

const Category = {
    CategoryCreate,
    CategoryEdit,
    CategoryList,
    CategoryLayout
}

export default Category
