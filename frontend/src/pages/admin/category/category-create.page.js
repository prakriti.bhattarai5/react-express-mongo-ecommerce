import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpPostRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";

const CategoryCreate= () =>{
    let navigate = useNavigate();
    let default_data = {
        title: "",
        slug : "",
        image : "",
        status : "inactive"
    }

    let CategoryValidation = Yup.object().shape({
        title : Yup.string().required('Title is required'),
        slug : Yup.string().required('Slug is required'),
        image : Yup.object().nullable(),
        status : Yup.string().required('Status is required')
    })

    let formik = useFormik({
        initialValues: default_data,
        validationSchema : CategoryValidation,
        onSubmit : async(values) => {
            try {
                console.log(values)
                const form_data = new FormData();

                Object.keys(values).forEach((key) => {
                    form_data.append(key, values[key]);
                  });
                let response = await httpPostRequest("/categories", form_data, true);
                if (response.status) {
                    toast.success(response.msg);
                    navigate('/admin/category');
                }
              } catch (err) {
                    toast.error(err.msg);
                    navigate('/admin/category');
              }
        }
    });


    return(<>
        <Container>
            <Row className="mt-4">
                <Col sm={{span:10}}>
                    <ToastContainer />
                    <h4>Create Category </h4>
                    <hr></hr>
                    <Form onSubmit={formik.handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="Enter Title" name="title" size="sm" onChange={formik.handleChange} required />
                            { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Slug</Form.Label>
                            <Form.Control type="text" placeholder="Enter Slug" name="slug" size="sm" onChange={formik.handleChange} required/>
                            { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="status" size="sm" onChange={formik.handleChange} required>
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="file" name="image" size="sm" onChange={(e)=> {
                                console.log(e.target.files[0])
                                formik.setValues({
                                        ...formik.values,
                                        image: e.target.files[0]
                                    })
                            }}/>
                            { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                        </Form.Group>
                        <img className="img-fluid" src={formik.values.image ?? URL.createObjectURL(formik.values.image)} alt="" />
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="success" size="md" type="submit" className="mr-10">
                                        Send
                                    </Button>
                                    <Button variant="danger" size="md" type="reset" className="ml-10">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default CategoryCreate