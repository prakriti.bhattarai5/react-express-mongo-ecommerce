import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpGetRequest, httpPutRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";

const CategoryEdit = () =>{
    let navigate = useNavigate();
    const { id } = useParams();
    const [category, setCategory] = useState({});

    let CategoryValidation = Yup.object().shape({
        title : Yup.string().required('Title is required'),
        slug : Yup.string().required('Slug is required'),
        status : Yup.string().required('Status is required'),
        image : Yup.object().nullable(),

    })

    let formik = useFormik({
        initialValues: {
            title: '',
            slug: '',
            status: '',
            image: '',

        },
        validationSchema : CategoryValidation,
        onSubmit : async(values) => {
            try {
                let form_data = new FormData();
                Object.keys(values).map((key) => {
                  form_data.append(key, values[key]);
                });

                let response = await httpPutRequest(`/categories/${id}`, values, true, true);
                if(response.status){
                    toast.success(response.msg);
                    navigate('/admin/category');
                }
                
              } catch (err) {
                    toast.error(err.msg);
                    navigate('/admin/category');

              }
        }
    });
    useEffect(() => {
        async function fetchData() {
            const response = await httpGetRequest(`/categories/${id}`);
            setCategory(response.result);
            
            formik.setValues({
                title: response.result.title,
                slug: response.result.slug,
                status: response.result.status,
                image: response.result.image,

            });
        }  
        fetchData(); 
      }, [id]);

    return(<>

        <Container>
            <Row className="mt-4">
                <Col sm={{span:10}}>
                    <ToastContainer />
                    <h4>Create Category </h4>
                    <hr></hr>
                    <Form onSubmit={formik.handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="Enter Title" name="title" size="sm" value={formik.values.title}
                             onChange={formik.handleChange}
                             required />
                            { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Slug</Form.Label>
                            <Form.Control type="text" placeholder="Enter Slug" name="slug" size="sm" value={formik.values.slug} onChange={formik.handleChange} required/>
                            { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="status" size="sm" onChange={formik.handleChange} value={formik.values.status} required>
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="file" name="image" size="sm" onChange={(e)=> {
                                formik.setValues({
                                        ...formik.values,
                                        image: e.target.files[0]
                                    })
                            }}/>

                            {formik.values.image && 
                                <img className="img-thumbail mt-3" src={`http://127.0.0.1:3005/uploads/category/${formik.values.image}`}  width="50" height={50}/>
                            }
                        </Form.Group>
                        <img className="img-fluid" alt="" />
                        <div className="d-grid gap-2">
                             <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Update
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default CategoryEdit