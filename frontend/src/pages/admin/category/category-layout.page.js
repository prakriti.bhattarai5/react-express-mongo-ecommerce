import { Outlet } from "react-router-dom"

const CategoryLayout = () => {
    return(<>
        <Outlet></Outlet>
    </>)
}
export default CategoryLayout