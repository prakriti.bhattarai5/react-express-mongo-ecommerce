import Banner from "./banner"
import AdminDashboard from "./admin-dashboard.page"
import BannerLayout from "./banner/banner-layout.page"
import Brand from "./brand"
import Product from "./product/index"
import User from "./user/index"
import Category from "./category/index"

const AdminPages = {
    Banner,
    AdminDashboard,
    BannerLayout,
    Brand,
    Product,
    User,
    Category
}

export default AdminPages