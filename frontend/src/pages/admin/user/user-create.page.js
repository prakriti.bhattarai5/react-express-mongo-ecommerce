import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpPostRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import TopElements from "../../../components/common/top_elements";

const UserCreate= () =>{
    let navigate = useNavigate();
    let default_data = {
        name: "",
        email : "",
        password : "",
        role : "",
        image : "",
        status : ""
    }

    let UserValidation = Yup.object().shape({
        name : Yup.string().required('Name is required'),
        email : Yup.string().required('Email is required'),
        password : Yup.string().required('Password is required'),
        role : Yup.string().required('Role is required'),
        image : Yup.object().nullable(),
        status : Yup.string().required('Status is required')
    })

    let formik = useFormik({
        initialValues: default_data,
        validationSchema : UserValidation,
        onSubmit : async(values) => {
            try {
                let data = new FormData();
                Object.keys(values).forEach((key) => {
                    data.append(key, values[key]);
                  });
                  
                let response = await httpPostRequest("/users", data, true,true);
                if(response.status) {
                    toast.success(response.msg);
                    navigate('/admin/user');
                }
                } catch(err){
                    toast.error(err.msg);
            }
        }
    });

    return(<>
        <Container>
        <ToastContainer/>

        <TopElements name="User" type="create"/>
            <Row className="mt-4">
                <Col sm={{span:9}}>
                    <Form onSubmit={formik.handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicTitle">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Name" name="name" size="sm" onChange={formik.handleChange} required />
                            { formik.errors.name &&  <em className="text-danger">{formik.errors.name}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter Email" name="email" size="sm" onChange={formik.handleChange} required />
                            { formik.errors.email &&  <em className="text-danger">{formik.errors.email}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter Password" name="password" size="sm" onChange={formik.handleChange} required />
                            { formik.errors.password &&  <em className="text-danger">{formik.errors.password}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicRole">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="role" size="sm" onChange={formik.handleChange} required>
                                <option value="admin">Admin</option>
                                <option value="seller">Seller</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        
                        <Form.Group className="mb-3" controlId="formBasicStatus">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="status" size="sm" onChange={formik.handleChange} required>
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicImage">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="file" name="image" size="sm" onChange={(e)=> {
                                console.log(e.target.files[0])
                                formik.setValues({
                                        ...formik.values,
                                        image: e.target.files[0]
                                    })
                            }}/>
                            { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                        </Form.Group>
                        <img className="img-fluid" src={formik.values.image ?? URL.createObjectURL(formik.values.image)} alt="" />
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Send
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default UserCreate