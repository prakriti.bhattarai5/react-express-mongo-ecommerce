import UserCreate from "./user-create.page"
import UserEdit from "./user-edit.page"
import UserLayout from "./user-layout.page"
import UserList from "./user-list.page"

const User = {
    UserCreate,
    UserEdit,
    UserList,
    UserLayout
}

export default User
