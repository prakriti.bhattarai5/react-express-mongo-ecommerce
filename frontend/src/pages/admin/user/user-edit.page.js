import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpGetRequest, httpPutRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import TopElements from "../../../components/common/top_elements";

const UserEdit = () =>{
    let navigate = useNavigate();
    const { id } = useParams();
    const parseId = JSON.parse(decodeURIComponent(id));
    const [user, setUser] = useState({});

    let UserValidation = Yup.object().shape({
        name : Yup.string().required('Name is required'),
        email : Yup.string().required('Email is required'),
        password : Yup.string().required('Password is required'),
        role : Yup.string().required('Role is required'),
        image : Yup.object().nullable(),
        status : Yup.string().required('Status is required')

    })

    let formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            role: '',
            status: '',
            image: '',

        },
        validationSchema : UserValidation,
        onSubmit : async(values) => {
            try {
                let form_data = new FormData();

                Object.keys(values).map((key) => {
                  form_data.append(key, values[key]);
                });

                let response = await httpPutRequest("/users/"+parseId, form_data, true, true);
                if(response.status){
                    toast.success(response.msg);
                    navigate('/admin/user')
                }
                
              } catch (err) {
                    toast.error(err.msg);
              }
        }
    });
    useEffect(() => {
        async function fetchData() {
            const response = await httpGetRequest("/users/"+parseId);
            if(response){
                setUser(response.result);
                formik.setValues({
                    name: response.result.name,
                    email: response.result.email,
                    password: response.result.password,
                    role: response.result.role,
                    status: response.result.status,
                    image: response.result.image,

                });
            }else{

            }
        }  
        fetchData(); 
      }, [parseId]);

    return(<>
        <Container>
            <ToastContainer />

            <TopElements name="User" type="Edit"/>
            <Row className="mt-4">
                <Col sm={{span:9}}>                    
                    <Form onSubmit={formik.handleSubmit}>
                        
                        <Form.Group className="mb-3" controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Name" name="name" size="sm" value={formik.values.name}
                             onChange={formik.handleChange}
                             required />
                            { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="text" placeholder="Enter Email" name="email" size="sm" value={formik.values.email}
                             onChange={formik.handleChange}
                             required />
                            { formik.errors.title &&  <em classEmail="text-danger">{formik.errors.email}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter Password" name="password" size="sm" value={formik.values.password}
                             onChange={formik.handleChange}
                             required />
                            { formik.errors.title &&  <em classPassword="text-danger">{formik.errors.password}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicRole">
                            <Form.Label>Role</Form.Label>
                            <Form.Control type="text" placeholder="Enter Role" name="role" size="sm" value={formik.values.role} onChange={formik.handleChange} required/>
                            { formik.errors.role && <em className="text-danger">{formik.errors.role}</em> }
                        </Form.Group>
                       
                        <Form.Group className="mb-3" controlId="formBasicStatus">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="status" size="sm" value={formik.values.status} onChange={formik.handleChange} required>
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="file" name="image" size="sm" onChange={(e)=> {
                                console.log(e.target.files[0])
                                formik.setValues({
                                        ...formik.values,
                                        image: e.target.files[0]
                                    })
                            }}/>
                            {formik.values.image && 
                                <img className="img-thumbail mt-3" src={`http://127.0.0.1:3005/uploads/user/${formik.values.image}`}  width="50" height={50}/>
                            }
                        </Form.Group>

                        <img className="img-fluid" alt="" />
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Send
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default UserEdit