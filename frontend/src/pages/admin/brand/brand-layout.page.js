import { Outlet } from "react-router-dom"

const BrandLayout = () => {
    return(<>
        <Outlet></Outlet>
    </>)
}
export default BrandLayout