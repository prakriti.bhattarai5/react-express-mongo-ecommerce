import BrandList from "./brand-list.page";
import BrandCreate from "./brand-create.page";
import BrandLayout from "./brand-layout.page";
import BrandEdit from "./brand-edit.page";


const Brand = {
    BrandList,
    BrandCreate,
    BrandLayout,
    BrandEdit

}
export default Brand