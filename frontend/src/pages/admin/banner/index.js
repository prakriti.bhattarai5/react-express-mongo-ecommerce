import BannerList from "./banner-list.page";
import BannerCreate from "./banner-create.page";
import BannerLayout from "./banner-layout.page";
import BannerEdit from "./banner-edit.page";


const Banner = {
    BannerList,
    BannerCreate,
    BannerLayout,
    BannerEdit

}
export default Banner