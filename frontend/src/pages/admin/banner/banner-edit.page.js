import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpGetRequest, httpPutRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import TopElements from "../../../components/common/top_elements";

const BannerEdit = () =>{
    let navigate = useNavigate();
    const { id } = useParams();
    const [banner, setBanner] = useState({});

    let BannerValidation = Yup.object().shape({
        title : Yup.string().required('Title is required'),
        slug : Yup.string().required('Slug is required'),
        status : Yup.string().required('Status is required'),
        image : Yup.object().nullable(),

    })

    let formik = useFormik({
        initialValues: {
            title: '',
            slug: '',
            status: '',
            image: '',

        },
        validationSchema : BannerValidation,
        onSubmit : async(values) => {
            try {
                let form_data = new FormData();

                Object.keys(values).map((key) => {
                  form_data.append(key, values[key]);
                });

                let response = await httpPutRequest(`/banners/${id}`, form_data, true, true);
                if(response.status){
                    toast.success(response.msg);
                    navigate('/admin/banner')
                }
                
              } catch (err) {
                    toast.error(err.msg);
              }
        }
    });
    useEffect(() => {
        async function fetchData() {
            const response = await httpGetRequest(`/banners/${id}`);
            if(response){
                setBanner(response.result);
                formik.setValues({
                    title: response.result.title,
                    slug: response.result.slug,
                    status: response.result.status,
                    image: response.result.image,

                });
            }else{

            }
        }  
        fetchData(); 
      }, [id]);

    return(<>
        <Container>
            <ToastContainer />

            <TopElements name="Banner" type="create"/>
            <Row className="mt-4">
                <Col sm={{span:9}}>                    
                    <Form onSubmit={formik.handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="Enter Title" name="title" size="sm" value={formik.values.title}
                             onChange={formik.handleChange}
                             required />
                            { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Slug</Form.Label>
                            <Form.Control type="text" placeholder="Enter Slug" name="slug" size="sm" value={formik.values.slug} onChange={formik.handleChange} required/>
                            { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                        </Form.Group>
                       
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Status</Form.Label>
                            <Form.Select name="status" size="sm" value={formik.values.status} onChange={formik.handleChange} required>
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicSlug">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="file" name="image" size="sm" onChange={(e)=> {
                                console.log(e.target.files[0])
                                formik.setValues({
                                        ...formik.values,
                                        image: e.target.files[0]
                                    })
                            }}/>
                            {formik.values.image && 
                                <img className="img-thumbail mt-3" src={`http://127.0.0.1:3005/uploads/banner/${formik.values.image}`}  width="50" height={50}/>
                            }
                        </Form.Group>

                        <img className="img-fluid" alt="" />
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Send
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default BannerEdit