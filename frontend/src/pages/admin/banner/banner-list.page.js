import { useEffect, useState } from "react";
import { Col, Row, Table } from "react-bootstrap"
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import TopElements from "../../../components/common/top_elements";
import { httpDeleteRequest, httpGetRequest } from "../../../services/axios.service";

const BannerList = () => {
    const [banners, setBanners] = useState([]);

    useEffect(() => {
      async function fetchData() {
        const response = await httpGetRequest("/banners");
        setBanners(response.result)
      }  
      fetchData();
    }, []);

    const handleDelete = async (itemId) => {
        try {
            console.log(itemId)
          // Send a HTTP DELETE request to delete the item from the database
          const response = await httpDeleteRequest(`/banners/${itemId}`);
          // Update the state of the component to remove the deleted item
          setBanners(banners.filter((item) => item._id !== itemId));
          if(response.status){
            toast.success(response.msg);
          }
        } catch (error) {
            toast.error(error.msg);
        }
      };
  
     return(<>
         <div className="container-fluid px-4">
            <TopElements name="Banner" type="create"/>
                
             <div className="card mb-4">
                <Row className="m-2">
                    <Col sm={10}></Col>
                    <Col sm={2}><Link to={'create'} className="btn-sm btn-primary" width={100}> <i className="fa-solid fa-plus"></i> Create</Link></Col>
                </Row>
                 <div className="card-body">
                     <Table striped bordered hover>
                         <thead>
                             <tr>
                             <th>#</th>
                             <th>Image</th>
                             <th>Title</th>
                             <th>Status</th>
                             <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             {banners && banners.map((item,index) => (
                                 <tr key={index}>
                                     <td>{++index}</td>
                                     <td>{item.image && <img src={`http://127.0.0.1:3005/uploads/banner/${item.image}`} alt="item" height="40" width="40"/>}</td>
                                     <td>{item.title}</td>
                                     <td>{item.status}</td>
                                     <td>
                                        <Link to={`edit/${item._id}`}> <i className="fa-solid fa-pen-to-square"></i> </Link> |  
                                        <a href="#" onClick={() => handleDelete(item._id)}>
                                             <i className="fa-solid fa-trash"></i>
                                        </a>
                                    </td>
                                 </tr>
                             ))}
                         </tbody>
                     </Table>
                 </div>
             </div>
         </div>
     </>)
}

export default BannerList