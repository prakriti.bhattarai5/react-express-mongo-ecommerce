import { Outlet } from "react-router-dom"

const BannerLayout = () => {
    return(<>
        <Outlet></Outlet>
    </>)
}
export default BannerLayout