import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpGetRequest, httpPutRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import TopElements from "../../../components/common/top_elements";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

const ProductEdit = () =>{
    let navigate = useNavigate();
    const { id } = useParams();
    let [product, setProduct] = useState([]);
    let [categories, setCategories] = useState([]);
    let [brands, setBrands] = useState([]);

    let ProductValidation = Yup.object().shape({
        title : Yup.string().required('Title is required'),
        slug : Yup.string().required('Slug is required'),
        image : Yup.object().nullable(),
        status : Yup.string().required('Status is required'),
        stock : Yup.string().required('Stock is required'),
        discount_type : Yup.string().nullable(),
        discount_value : Yup.string().nullable(),
        is_featured : Yup.string(),
        brand : Yup.string().nullable(),
        category : Yup.string().nullable(),
        description:Yup.string().nullable()

    })

    let formik = useFormik({
        initialValues: {
            title: "",
            slug : "",
            image : "",
            status : "",
            is_featured : "",
            category : "",
            brand : "",
            discount_value : "",
            discount_type : "",
            stock : "",
            description : "",
        },
        validationSchema : ProductValidation,
        onSubmit : async(values) => {
            try {
                let form_data = new FormData();

                Object.keys(values).map((key) => {
                  form_data.append(key, values[key]);
                });

                let response = await httpPutRequest(`/products/${id}`, form_data, true, true);
                if(response.status){
                    toast.success(response.msg);
                    navigate('/admin/product')
                }
                
              } catch (err) {
                    toast.error(err.msg);
              }
        }
    });
    useEffect(() => {
        async function fetchData() {
            let response = await httpGetRequest(`/products/${id}`);
            let categories_response = await httpGetRequest("/categories");
            let brands_response = await httpGetRequest("/brands");
            if(response){
                setProduct(response.result);
                setCategories(categories_response.result);
                setBrands(brands_response.result);
                formik.setValues({
                    title: response.result.title,
                    slug: response.result.slug,
                    status: response.result.status,
                    image: response.result.image,

                });
            }else{

            }
        }  
        fetchData(); 
      }, [id]);

    return(<>
        <Container>
            <ToastContainer />

            <TopElements name="Product" type="create"/>
            <Row className="mt-4">
                <Col sm={{span:9}}>                    
                <Form onSubmit={formik.handleSubmit}>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Title" name="title" size="sm" value={formik.values.title} onChange={formik.handleChange} required />
                                    { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Slug</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Slug" name="slug" value={formik.values.slug} size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <CKEditor
                                    editor={ ClassicEditor }
                                    // data={formik.values.description && formik.values.description}
                                    onReady={ editor => {
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        formik.setValues({...formik.values,description:data})
                                    } }
                                    name="description"
                                    onBlur={ ( event, editor ) => {
                                    } }
                                    onFocus={ ( event, editor ) => {
                                    } }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control type="number" placeholder="Enter Price" value={formik.values.price} name="price" size="sm" onChange={formik.handleChange} required />
                                    { formik.errors.price &&  <em className="text-danger">{formik.errors.price}</em> }
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Stock</Form.Label>
                                    <Form.Control type="number" placeholder="Enter Stock" name="stock" size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.stock && <em className="text-danger">{formik.errors.stock}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Discount Type</Form.Label>
                                    <Form.Select name="discount_type" value={formik.values.discount_type} size="sm" onChange={formik.handleChange} required>
                                            <option value="percent">-- Select --</option>
                                            <option value="percent">Percent</option>
                                            <option value="flat">Flat</option>
                                        </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Discount Value</Form.Label>
                                    <Form.Control type="number" value={formik.values.discount_value} placeholder="Discount Value" name="discount_value" size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.discount_value && <em className="text-danger">{formik.errors.discount_value}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Category</Form.Label>
                                    <Form.Select name="category" size="sm" onChange={formik.handleChange} required>
                                            <option value="">-- Select --</option>
                                            {categories && categories.map((item,index) => (
                                                <option value={item._id}>{item.title}</option>
                                            ))}
                                        </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Brand</Form.Label>
                                    <Form.Select name="brand" size="sm" onChange={formik.handleChange} required>
                                        <option value="">-- Select --</option>
                                        {brands && brands.map((item,index) => (

                                            <option value={item._id}>{item.title}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                        </Row>
                            <Row>
                                <Col sm={{span:6}}>
                                    <Form.Group className="mb-3" controlId="formBasicSlug">
                                        <Form.Label>Status</Form.Label>
                                        <Form.Select name="status" size="sm" onChange={formik.handleChange} required>
                                            <option value="inactive">Inactive</option>
                                            <option value="active">Active</option>
                                        </Form.Select>
                                    </Form.Group>
                                        
                                        { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                                </Col>
                            </Row>
                            
                            <Form.Group className="mb-3" controlId="formBasicSlug">
                                <Form.Label>Is Featured</Form.Label>
                                <Form.Check
                                    aria-label="option 1"
                                    name="is_featured"
                                    onChange={formik.handleChange}
                                    checked={formik.values.is_featured} 
                                    value={formik.values.is_featured}// Add this line to set the initial checked state
                                />
                            </Form.Group>
                            {formik.errors.is_featured && <em className="text-danger">{formik.errors.is_featured}</em>}

                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                
                            <Form.Label>Upload Images</Form.Label>
                                <Form.Control type="file" name="images" size="sm" onChange={(e)=> {
                                    //console.log(e.target.files[0])
                                    formik.setValues({
                                            ...formik.values,
                                            images: e.target.files[0]
                                        })
                                }} multiple/>
                                { formik.errors.image && <em className="text-danger">{formik.errors.image}</em> }
                            </Form.Group>
                        {/* <img className="img-fluid" src={formik.values.image ?? URL.createObjectURL(formik.values.image)} alt="" /> */}
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Send
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default ProductEdit