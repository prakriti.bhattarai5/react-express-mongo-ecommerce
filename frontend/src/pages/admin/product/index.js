import ProductCreate from "./product-create.page"
import ProductEdit from "./product-edit.page"
import ProductLayout from "./product-layout.page"
import ProductList from "./product-list.page"

const Product = {
    ProductCreate,
    ProductEdit,
    ProductList,
    ProductLayout
}

export default Product
