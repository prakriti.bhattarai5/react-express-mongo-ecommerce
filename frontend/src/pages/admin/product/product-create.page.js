import { useFormik } from "formik"
import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { ToastContainer, toast } from "react-toastify"
import { httpGetRequest, httpPostRequest } from "../../../services/axios.service"
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import TopElements from "../../../components/common/top_elements";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useEffect, useState } from "react";

const ProductCreate= () =>{
    let navigate = useNavigate();
    const [categories,setCategories]= useState([]);
    const [brands,setBrands]= useState([]);
    let default_data = {
        title: "",
        slug : "",
        images : "",
        status : "",
        is_featured : "",
        category : "",
        brand : "",
        discount_value : "",
        discount_type : "",
        quantity : "",
        description : "",
    }
    useEffect(()=>{
        async function fetchData() {
            let rcategories = await httpGetRequest("/categories");
            let rbrands = await httpGetRequest("/brands");
            setCategories(rcategories.result)
            setBrands(rbrands.result)
        }  
        fetchData();
    },[])
    let ProductValidation = Yup.object().shape({
        title : Yup.string().required('Title is required'),
        slug : Yup.string().required('Slug is required'),
        images : Yup.object().nullable(),
        status : Yup.string().required('Status is required'),
        quantity : Yup.string().required('Stock is required'),
        discount_type : Yup.string().nullable(),
        discount_value : Yup.string().nullable(),
        is_featured : Yup.string(),
        brand : Yup.string().nullable(),
        category : Yup.string().nullable(),
        description:Yup.string().nullable()
    })

    let formik = useFormik({
        initialValues: default_data,
        validationSchema : ProductValidation,
        onSubmit : async(values) => {
            try {
                console.log(values)
                let data = new FormData();
                Object.keys(values).forEach((key) => {
                    data.append(key, values[key]);
                  });
                  
                let response = await httpPostRequest("/products", data, true,true);
                if(response.status) {
                    toast.success(response.msg);
                    navigate('/admin/product');
                }
                } catch(err){
                    toast.error(err.msg);
            }
        }
    });

    return(<>
        <Container>
        <ToastContainer/>

        <TopElements name="Product" type="create"/>
            <Row className="mt-4">
                <Col sm={{offset:1,span:10}}>
                    <Form onSubmit={formik.handleSubmit}>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Title" name="title" size="sm" onChange={formik.handleChange} required />
                                    { formik.errors.title &&  <em className="text-danger">{formik.errors.title}</em> }
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Slug</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Slug" name="slug" size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <CKEditor
                                    editor={ ClassicEditor }
                                    data="<p>Enter Description</p>"
                                    onReady={ editor => {
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        formik.setValues({...formik.values,description:data})
                                    } }
                                    name="description"
                                    onBlur={ ( event, editor ) => {
                                    } }
                                    onFocus={ ( event, editor ) => {
                                    } }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control type="number" placeholder="Enter Price" name="price" size="sm" onChange={formik.handleChange} required />
                                    { formik.errors.price &&  <em className="text-danger">{formik.errors.price}</em> }
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Stock</Form.Label>
                                    <Form.Control type="number" placeholder="Enter Quantity" name="quantity" size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.quantity && <em className="text-danger">{formik.errors.quantity}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Discount Type</Form.Label>
                                    <Form.Select name="discount_type" size="sm" onChange={formik.handleChange} required>
                                            <option value="percent">-- Select --</option>
                                            <option value="percent">Percent</option>
                                            <option value="flat">Flat</option>
                                        </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                    <Form.Label>Discount Value</Form.Label>
                                    <Form.Control type="text" placeholder="Discount Value" name="discount_value" size="sm" onChange={formik.handleChange} required/>
                                    { formik.errors.discount_value && <em className="text-danger">{formik.errors.discount_value}</em> }
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Category</Form.Label>
                                    <Form.Select name="category" size="sm" onChange={formik.handleChange} required>
                                            <option value="">-- Select --</option>
                                            {categories && categories.map((item,index) => (
                                                <option value={item._id}>{item.title}</option>
                                            ))}
                                        </Form.Select>
                                </Form.Group>
                            </Col>
                            <Col sm={{span:6}}>
                                <Form.Group className="mb-3" controlId="formBasicTitle">
                                    <Form.Label>Brand</Form.Label>
                                    <Form.Select name="brand" size="sm" onChange={formik.handleChange} required>
                                        <option value="">-- Select --</option>
                                        {brands && brands.map((item,index) => (

                                            <option value={item._id}>{item.title}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Col>
                        </Row>
                            <Row>
                                <Col sm={{span:6}}>
                                    <Form.Group className="mb-3" controlId="formBasicSlug">
                                        <Form.Label>Status</Form.Label>
                                        <Form.Select name="status" size="sm" onChange={formik.handleChange} required>
                                            <option value="inactive">Inactive</option>
                                            <option value="active">Active</option>
                                        </Form.Select>
                                    </Form.Group>
                                        
                                        { formik.errors.status && <em className="text-danger">{formik.errors.status}</em> }
                                </Col>
                                {/* <Col sm={{span:6}}>
                                    <Form.Group className="mb-3" controlId="formBasicSlug">
                                        <Form.Label>Is Featured</Form.Label>
                                        <Form.Check aria-label="option 1" />Yes
                                    </Form.Group>
                                </Col> */}
                            </Row>
                            
                            <Form.Group className="mb-3" controlId="formBasicSlug">
                                <Form.Label>Is Featured</Form.Label>
                                <Form.Check
                                    aria-label="option 1"
                                    name="is_featured"
                                    onChange={formik.handleChange}
                                    checked={formik.values.is_featured} 
                                    value={formik.values.is_featured}// Add this line to set the initial checked state
                                />
                            </Form.Group>
                            {formik.errors.is_featured && <em className="text-danger">{formik.errors.is_featured}</em>}

                                <Form.Group className="mb-3" controlId="formBasicSlug">
                                
                            <Form.Label>Upload Images</Form.Label>
                                <Form.Control type="file" name="images" size="sm" onChange={(e)=> {
                                    console.log(e.target.files[0])
                                    formik.setValues({
                                            ...formik.values,
                                            images: e.target.files[0]
                                        })
                                }} multiple/>
                                { formik.errors.slug && <em className="text-danger">{formik.errors.slug}</em> }
                            </Form.Group>
                        <img className="img-fluid" src={formik.values.images ?? URL.createObjectURL(formik.values.images)} alt="" />
                        <div className="d-grid gap-2">
                            <Row>
                                <Col sm={{span:10}}>
                                    <Button variant="primary" size="md" type="submit" className="m-1">
                                        Send
                                    </Button>
                                    <Button variant="secondary" size="md" type="reset" className="">
                                        Reset
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    </>)
}

export default ProductCreate