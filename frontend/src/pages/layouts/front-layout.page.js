import Front from "../../components/front";
import { Outlet } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setCart } from "../../reducer/cart.reducer";

const FrontLayout = () => {
    let dispatch = useDispatch();
    let cart = JSON.parse(localStorage.getItem("cart_8")) || [];
    dispatch(setCart(cart))

    return(<>
            <Front.NavbarComponent />
            <Outlet></Outlet>
            <Front.FooterComponent />
        </>)
}

export default FrontLayout;