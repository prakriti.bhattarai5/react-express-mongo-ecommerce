import { useEffect, useState } from "react";
import {Form, Button, Container, Row, Col} from "react-bootstrap";
import {NavLink, useNavigate} from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { httpPostRequest } from "../../services/axios.service";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FacebookLogin from 'react-facebook-login';

const LoginPage = () => {
    const navigate = useNavigate();
    let default_data = {
        email: "",
        password : "",
        remember_me : false,
    }
    let [loading, setLoading] = useState(false);

    const responseFacebook = async(response)=> {
        try{
            let loginRequest = await httpPostRequest('/facebookLogin',response);

        }catch(err){
            console.log(err)
        }
    }
    let loginValidation = Yup.object().shape({
        email : Yup.string().email('Invalid Email Format').required('Email is required'),
        password : Yup.string().required('Password is required')
    })

    let formik = useFormik({
        initialValues: default_data,
        validationSchema : loginValidation,
        onSubmit : async(values) => {
            try{
                let response = await httpPostRequest('/login', values)
                console.log(response)
                if(response.status){
                    let user_info = {
                        id : response.result._id,
                        name : response.result.name,
                        image : response.result.image,
                        role : response.result.role
                    }
                    localStorage.setItem('access_token', response.access_token);
                    localStorage.setItem('_au', JSON.stringify(user_info));
                    let token = localStorage.getItem('access_token')
                    if(token) {
                        toast.success('Success')
                        navigate('/admin');
                    }else{
                        throw('Could not log in try again !')
                    }
                }else {
                    throw('Could not log in try again !')
                }
            }catch(err) {
                toast.error(err.response.data.msg)
                console.log(err.response.data.msg)
            }
        }
    });

    useEffect(()=>{
        let token = localStorage.getItem('access_token')
        if(token) {
             let user_info = JSON.parse(localStorage.getItem('_au'));
             navigate('/admin')
         }
     },[navigate])

    return(<>

        { loading ? 'Loading...' : <Container>
            <Row className="mt-4">
                <Col sm={{offset:3, span:6}}>
                    <ToastContainer />

                    <h4>Please Login </h4>
                    <hr></hr>
                    <Form onSubmit={formik.handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" name="email" size="sm" onChange={formik.handleChange} required />
                           { formik.errors.email &&  <em className="text-danger">{formik.errors.email}</em> }
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" name="password" size="sm" onChange={formik.handleChange} required/>
                            
                            { formik.errors.password && <em className="text-danger">{formik.errors.password}</em> }

                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Remember Me" name="remember_me" onChange={ (e) => {
                                if(e.target.checked) {
                                    formik.setValues({
                                        ...formik.values,
                                        remember_me : true
                                    })
                                }else {
                                    formik.setValues({
                                        ...formik.values,
                                        remember_me : false
                                    })
                                }
                            }} />
                        </Form.Group>
                        <Row className="mt-3 mb-4">
                                <Button variant="primary" className="m-2" style={{ width: "100px", height: "35px",}} size="sm" type="submit">
                                    Login
                                </Button> 
                                <Button className="m-2" style={{ width: "100px", height: "35px",}} variant="secondary" width="200" size="sm" type="reset">
                                    Reset
                                </Button>                               
                                <NavLink to="/register" style={{color:'#364968'}}> Don't have account create a new account? Click Here</NavLink>
                        </Row>
                       
                    </Form>

                    {/* <FacebookLogin
                        appId="235344285507105"
                        autoLoad={false}
                        cssClass="my-facebook-button-class"
                        callback={responseFacebook}
                    /> */}
                </Col>
            </Row>
        </Container>
        }  
    </>);
}
export default LoginPage