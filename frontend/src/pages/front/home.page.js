import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {Carousel,Card, Button, Row, Container, Col, InputGroup, Form} from "react-bootstrap";
import { httpGetRequest } from "../../services/axios.service";
import Cart from "./cart.page";
import { Link, useNavigate } from "react-router-dom";
import {useDispatch, useSelector} from "react-redux"
import { addToCart } from "../../reducer/cart.reducer";

const HomePage = () =>{

    let navigate = useNavigate();
    let [banners, setBanners] = useState(null);
    let [categories, setCategories] = useState([]);
    let [brands, setBrands] = useState([]);
    let [products, setProducts] = useState([]);
    let [cartItems, setCartItems] = useState([]);
    let [qty, setQty] = useState(0);
    const dispatch = useDispatch();

    const addQty=(e)=>{
        setQty(e.target.value)
    }
    let cart = useSelector((store)=>{
        return store.cart;
    })
    const handleSubmit=(e)=>{
        e.preventDefault();
        console.log(e)
        let product_id = e.target.id.value
        dispatch(addToCart({qty:qty,product_id:product_id}))
    }
    useEffect(() => {
        async function getBanner(){
            try{
                let banner = await httpGetRequest('/banners');
                let category = await httpGetRequest('/categories');
                let brand = await httpGetRequest('/brands');
                let product = await httpGetRequest('/products');
                let cartItem = localStorage.getItem('cart');
                
                setBanners(banner.result);
                setBrands(brand.result);
                setCategories(category.result);
                setProducts(product.result);
                if (cartItem) {
                    setCartItems(JSON.parse(cartItem));
                }

            }catch(err) {

            }
        }
       getBanner();

    },[]);
        return(<>
            <Carousel>
                {banners && banners.map(item => (
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src={`http://127.0.0.1:3005/uploads/banner/${item.image}`}
                        alt="First slide" height="500" width="100%"
                        />
                        <Carousel.Caption className="text-secondary">
                            
                        </Carousel.Caption>
                    </Carousel.Item>
                ))}
                
            </Carousel>
            <Container>
                <Row className="mt-5 mb-5">
                    {categories && categories.map(item => (

                        <Col sm={4}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" src={`http://127.0.0.1:3005/uploads/category/${item.image}`} height="200" width="100%"/>
                            </Card>
                        </Col>
                    ))}
                </Row>

                <h3>Popular Products</h3>
                <Row className="mt-5 mb-5">
                    {products && products.map(item => (

                        <Col sm={3}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" height={200} className="img-responsive" style={{width:'100%'}} src={`http://127.0.0.1:3005/uploads/products/${item.images[0]}`} />
                                <Card.Body>
                                
                                <Row>

                                    <Col sm={9}>
                                        <Card.Title>
                                            <Link to={`products/${item.slug}`}>{item.title}</Link>
                                        </Card.Title>
                                        <Card.Text>

                                            <b className="text-bold">${item.price}{item.discount && item.discount_value}</b>

                                        </Card.Text>
                                    </Col>
                                    <Col sm={3} className="mt-2">

                                       
                                        

                                    </Col>
                                    <form onSubmit={handleSubmit}>
                                            {/* <Form.Control name="qty" min="1" type="number" required placeholder="Enter Quantity" value={qty}
                                            onChange = {addQty}
                                            />
                                            <Form.Control type="hidden" name="id" value={item._id} /> */}
                                            <Button size="sm" type="submit" style={{backgroundColor:'#DAA21B',border:'none'}}>
                                                {/* <i class="fas fa-shopping-cart"> </i> */}
                                                Add Cart
                                            </Button>
                                        </form>
                                </Row>
                                </Card.Body>

                            </Card>
                        </Col>
                    ))}
                    
                </Row>

                <h3>Featured Brands</h3>
                <Row className="mt-5 mb-5">
                    {brands && brands.map(item => (

                        <Col sm={2}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" height={100} className="img-responsive" style={{width:'100%'}} src={`http://127.0.0.1:3005/uploads/brand/${item.image}`} />
                                <Card.Body>
                        
                                </Card.Body>

                            </Card>
                        </Col>
                    ))}
                    
                </Row>
            </Container>
        </>)
    }
export default HomePage