import { getType } from "@reduxjs/toolkit";
import { useEffect, useState } from "react";
import { Button, Card, Carousel, CarouselItem, Col, Container, Form, Row, ToastContainer } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom"
import { toast } from "react-toastify";
import { addToCart, updateCart, updateItemInCart } from "../../reducer/cart.reducer";
import { httpGetRequest } from "../../services/axios.service";

const ProductSingle = ()=> {

    let { slug } = useParams();
    let [product,setProduct]= useState([]);
    let [products,setProducts]= useState([]);
    let [cartItems, setCartItems] = useState([]);
    let [quty, setQty] = useState(0);
    const dispatch = useDispatch();

    let addQty=(e)=>{
        setQty(e.target.value)
    }
    let cart = useSelector((store)=>{
        return store.cart;
    })

    let addItemToCart = () => {
        let index = null;
        let all_items = cart.cart;

        all_items &&
          all_items.map((cart_item, indx) => {
            if (product._id === cart_item.product_id) {
              index = indx;
            }
          });

        if (index !== null) {
            console.log(all_items[index]['qty'])
          let total_qty = Number(all_items[index]['qty']) + 1;
          console.log(total_qty);
          dispatch(updateItemInCart({ index: index, qty: total_qty }));

        } else {
          all_items = [
            ...all_items,
            {
              qty: 1,
              product_id: product._id,
            },
          ];
          console.log(all_items)
          dispatch(updateCart(all_items));
          toast.success("Your cart has been updated")
        }
      };


      const handleSubmit = (e) => {
        e.preventDefault();

        const product_id = e.target.id.value;
        let qty = quty
        dispatch(addToCart({ qty, product_id }));
        toast.success("Item has been added to the cart")

      };

    useEffect(() => {
      async function fetchData() {
        let response = await httpGetRequest(`/products/${slug}`);
        let response_products = await httpGetRequest(`/products`);
        setProduct(response.result);
        setProducts(response_products.result);
        console.log(response.result);
      }
      fetchData();
    }, []);
    return (<>
        <Container>
            <Row className="m-5">
                <Col sm={6}>
                    <h3></h3>
                    { product.images && product.images.map((item)=>{

                      return  <Carousel slide={false}>
                      <Carousel.Item>
                      <img src={`http://127.0.0.1:3005/uploads/products/${item}`} alt="item" height="300" width="400" />
                      </Carousel.Item>
                        </Carousel>
                    })}
                </Col>

                <Col sm={6}>
                    <h3 style={{color:"#887456"}}>Product Description</h3>
                    <h4>{product.title}</h4>
                    <p><span>{product.discount_value && product.discount_value}</span>{product.after_discount && product.after_discount}</p>
                    <p className="text-secondary">Before: $<s>{product.price}</s></p>
                    <Form onSubmit={handleSubmit}>
                      <Row>

                        <Col>
                          <Form.Control name="qty" min="1" type="number" required placeholder="Enter Quantity" value={quty}
                           onChange = {addQty} />
                          <Form.Control type="hidden" name="id" value={product._id} />
                        </Col>
                        <Col>
                         
                          <Button size="sm" type="submit" style={{backgroundColor:'#DAA21B',border:'none'}}>
                              <i class="fas fa-shopping-cart"> </i>
                          </Button>
                        </Col>
                        </Row>
                        
                    </Form>

                    {/* <Button size="sm" type="submit" onClick={addItemToCart} style={{backgroundColor:'#DAA21B',border:'none'}}>
                      Add To cart
                    </Button> */}
                </Col>

                <Col className="mt-5">
                  <h4>Description</h4>
                  <hr></hr>

                  <p>{product.description}</p>

                </Col>
            </Row>
            <h3>Popular Products</h3>
                <Row className="mt-5 mb-5">
                    {products && products.filter(item => item._id !== product._id).map(item => (

                        <Col sm={3}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" height={200} className="img-responsive" style={{width:'100%'}} src={`http://127.0.0.1:3005/uploads/products/${item.images[0]}`} />
                                <Card.Body>

                                <Row>
                                    <Col sm={9}>
                                        <Card.Title>
                                            <Link to={`/products/${item.slug}`}>{item.title}</Link>
                                        </Card.Title>
                                        <Card.Text>

                                            <b className="text-bold">${item.price}{item.discount && item.discount_value}</b>

                                        </Card.Text>
                                    </Col>
                                    <Col sm={3} className="mt-2">
                                    </Col>

                                    <Button size="sm" type="submit" onClick={addItemToCart} style={{backgroundColor:'#DAA21B',border:'none'}}>
                                      Add To cart
                                    </Button>
                                </Row>
                                </Card.Body>

                            </Card>
                        </Col>
                    ))}

                </Row>

        </Container>
    </>)

}
export default ProductSingle