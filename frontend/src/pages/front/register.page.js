import { useState } from "react";
import { Form, Button, Container, Row, Col, ToastContainer } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { httpPostRequest } from "../../services/axios.service";

const RegisterPage = () => {
    const navigate = useNavigate();

    let [data, setData] = useState({
        name : "",
        email: "",
        password: "",
        status: "",
        role: "",
    })
    let [err, setErr] = useState({
        email: "",
        password: "",
        role: "",
        status: "",
        name: "",
    })

    const validateData = (field, value) => {
        let msg = "";
        switch (field) {
            case "name":
                msg = !value ? "Name is required" : '';
                break;
            case "email":
                msg = !value ? "Email is required" : '';
                break;
            case "password":
                msg = !value ? "Password is required" : '';
                break;
            case "role":
                    msg = !value ? "Role is required" : '';
                    break;
            case "status":
                    msg = !value ? "Status is required" : '';
                    break;
            default:
                break;
        }
        setErr({
            ...err,
            [field]: msg
        })

    }

    const handleChange = (e) => {
        console.log(e.target)
        let { name, value } = e.target;
        setData({
            ...data,
            [name]: value
        })
        validateData(name, value)
        //console.log(data)
    }
    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            let response = await httpPostRequest('/register',data, false, false);
            console.log(response);
            if(response.status){
                toast.success(response.data.msg)
                navigate('/admin');

            }
        }catch(err){
            toast.error(err.response.data.msg)
        }
        console.log(data)
        
    }
    return (<>
        {data.loading ? 'Loading...' : <Container>
            <ToastContainer></ToastContainer>
            <Row className="mt-4">
                <Col sm={{ offset: 3, span: 6 }}>
                    <h4>Register </h4>
                    <hr></hr>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="name" placeholder="Enter Name" name="name" size="sm" onChange={handleChange} required />
                            <em className="text-danger">{err?.name}</em>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter Email" name="email" size="sm" onChange={handleChange} required />
                            <em className="text-danger">{err?.email}</em>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" name="password" size="sm" required onChange={handleChange} />
                            <em className="text-danger">{err?.password}</em>

                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicRole">
                            <Form.Label>Role</Form.Label>
                            <Form.Select aria-label="Default select example" name="role" size="sm" onChange={handleChange}>
                                <option>--Select--</option>
                                <option value="admin">Admin</option>
                                <option value="seller">Seller</option>
                            </Form.Select>
                            <em className="text-danger">{err?.role}</em>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicRole">
                            <Form.Label>Status</Form.Label>
                            <Form.Select aria-label="Default select example" name="status" size="sm" onChange={handleChange} >
                                <option value="inactive">Inactive</option>
                                <option value="active">Active</option>
                            </Form.Select>
                            <em className="text-danger">{err?.status}</em>
                        </Form.Group>

                        <div className="d-grid gap-2">
                            <Button variant="primary" size="sm" type="submit">
                                Submit
                            </Button>
                        </div>
                    </Form>
                    <div className="text-center">
                        Or
                        <br></br>
                        <NavLink to="/login">Login</NavLink>
                    </div>

                </Col>
            </Row>
        </Container>
        
        }
        
    </>);
}
export default RegisterPage;