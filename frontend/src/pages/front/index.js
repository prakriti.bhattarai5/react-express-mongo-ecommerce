import HomePage from "./home.page";
import LoginPage from "./login.page";
import RegisterPage from "./register.page";
import ErrorPage from "./error.page";
import Cart from "./cart.page";
import ProductSingle from "./product_single.page";

const FrontPage = {
    HomePage,
    LoginPage,
    RegisterPage,
    ErrorPage,
    Cart,
    ProductSingle
    
}

export default FrontPage