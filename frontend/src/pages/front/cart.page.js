import { useCallback, useEffect, useState } from "react"
import { Col, Container, NavLink, Row, Table } from "react-bootstrap";
import {useSelector} from "react-redux"
import { httpPostRequest } from "../../services/axios.service";

 
const Cart =() => {

    // let [cartItems, setCartItems] = useState([]);
    let [cartDetails, setCartDetails] = useState([]);
    let cartItems = useSelector((store)=>{
        return store.cart.cart;
    });
    const getCartDetail = useCallback(async ()=>{
        try{
            let cartDetail = await httpPostRequest("/cart/detail",cartItems);
            if(cartDetail){
                console.log(cartDetail)
                setCartDetails(cartDetail.result)
            }
        }catch(error){

        }
        console.log(cartItems)
        let prod_ids = cartItems.map((item)=>item.product_id);
    },[cartItems])

    useEffect(()=>{
        getCartDetail()
    },[getCartDetail])
    // const calculateTotalPrice = () => {
    //     let totalPrice = 0;
    //     cartItems && cartItems.forEach(item => {
    //       totalPrice += item.price;
    //     });
    //     return totalPrice.toFixed(2);
    // };
    
    // useEffect(()=>{
    //     let getCartItems = localStorage.getItem('cart');
    //     console.log(getCartItems)
    //     setCartItems(JSON.parse(getCartItems));
    // },[])
    // const removeCartItem = (item) => {
    //     const updatedCartItems = cartItems.filter((cartItem) => cartItem._id !== item._id);
        
    //     console.log(updatedCartItems)
    //     setCartItems(updatedCartItems);
    //     localStorage.setItem("cart", JSON.stringify(updatedCartItems));

       
    // };
    return(<>
        <Container>
            <Row className="mt-4">
                <h3>Cart Items</h3>   
                <Table bordered>
                         <thead>
                             <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             {cartDetails && cartDetails.map((item,index) => (
                                 <tr>
                                     <td>{++index}</td>
                                     <td>{item.title}</td>
                                     <td>{item.qty}</td>
                                     <td>{item.total}</td>
                                    <td> 
                                        <a href="#" >
                                            Remove
                                        </a>
                                    </td>
                                 </tr>
                             ))}
                         </tbody>
                         <tfoot>
                            <tr>
                                <th>#</th>
                                <th></th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                         </tfoot>
                     </Table>
                     <NavLink to="/checkout" className="text-right">Checkout</NavLink>

            </Row>
        </Container>
    </>)

}

export default Cart