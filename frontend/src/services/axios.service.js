import axios from "axios";

const axiosInstance = axios.create({
    baseUrl : "http://localhost:3005/api/v1",
    timeout : 30000,
    timeoutErrorMessage : "Server Timeout",
    headers : {
        "content-type" : "application/json",
    }
})

let headers = {}
const setHeader = (is_strict, form_data = false) => {
    headers = {
        headers : {
            "content-type" : "application/json"
        }
    }
    if(is_strict) {
        let token = localStorage.getItem('access_token');
        headers= {
            headers : {
                ...headers.headers,
                "authorization" : "Bearer "+token
            }
        }
    }
    if(form_data){
        headers = {
            headers : {
                ...headers.headers,
                "content-type" : "multipart/form-data"
            }
        }
    }
}

axiosInstance.interceptors.response.use((response)=>{
    if(response.status === 200 || response.status === 201) {
        return response.data;
    }else {
        return response;
    }
})

export const httpPostRequest = (url, data, is_strict=false, form_data=false) => {
    setHeader(is_strict,form_data)
    return axiosInstance.post('http://localhost:3005/api/v1'+url, data,headers);
}

export const httpGetRequest = (url, data=false, is_strict=false) => {
    setHeader(is_strict)
    console.log(url)
    return axiosInstance.get('http://localhost:3005/api/v1'+url, data,headers);
}

export const httpPutRequest = (url, data, is_strict=false, form_data=false) => {
    setHeader(is_strict,form_data)
    return axiosInstance.put('http://localhost:3005/api/v1'+url, data,headers);
}

export const httpDeleteRequest = (url, data, is_strict) => {
    setHeader(is_strict=false)
    return axiosInstance.delete('http://localhost:3005/api/v1'+url, data, headers);
}