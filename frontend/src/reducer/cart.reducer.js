import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    cart:[]
}

export const CartSlicer = createSlice({
    name:"cart",
    initialState:initialState,
    reducers:{
        addToCart:(state, action)=>{
            let prev_cart = JSON.parse(localStorage.getItem('cart_8')) || []
            if(!prev_cart || prev_cart && prev_cart.length <= 0){
                prev_cart.push(action.payload)
            }else {
                let index = null;
                prev_cart.map((item,ind)=>{
                    if(item.product_id === action.payload.product_id){
                        index = ind
                    }
                })
                if(index === null){
                    prev_cart.push(action.payload);
                }else {
                    prev_cart[index].qty = action.payload.qty
                }
            }
            state.cart=prev_cart;
            localStorage.setItem("cart_8",JSON.stringify(prev_cart))
        },
        updateCart: (state, action) => {
            state.cart = action.payload
            localStorage.setItem("cart_8",JSON.stringify(action.payload));

          },
        updateItemInCart: (state, action) => {
            const { index, qty } = action.payload;

            // create a copy of the cart array
            const all_items = [...state.cart];
            
            // update the quantity of the item at the specified index
            all_items[index].qty = qty;
            
            // update the state
            state.cart = all_items;
            localStorage.setItem("cart_8",JSON.stringify(state.cart))
            
          },          
        setCart:(state, action)=>{
            state.cart = action.payload;
        }

    }
})

export const {addToCart,updateCart,updateItemInCart,setCart} = CartSlicer.actions

export default CartSlicer.reducer